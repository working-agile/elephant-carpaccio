﻿using TechTalk.SpecFlow;
using FluentAssertions;


namespace ElephantCarpaccioApp
{
    [Binding]
    public class BankAccountStepDefinitions
    {
        private readonly ScenarioContext _scenarioContext;

        public BankAccountStepDefinitions(ScenarioContext scenarioContext)
        {
            _scenarioContext = scenarioContext;
        }

        [Given(@"bla bla")]
        public void GivenBlaBla()
        {
            _scenarioContext.Pending();
        }

        [When(@"blub")]
        public void WhenBlub()
        {
            _scenarioContext.Pending();
        }

        [Then(@"blip")]
        public void ThenBlip()
        {
            _scenarioContext.Pending();
        }
    }
}
